class CreateTodos < ActiveRecord::Migration[5.1]
  def change
    create_table :todos do |t|
      t.datetime :date
      t.datetime :due_time
      t.string :description
      t.integer :status, default: 0

      t.timestamps
    end
  end
end
