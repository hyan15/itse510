json.extract! todo, :id, :date, :due_time, :description, :status, :created_at, :updated_at
json.url todo_url(todo, format: :json)
