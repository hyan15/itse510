class Todo < ApplicationRecord
  enum status: [:created, :done]

  validates :date, presence: true
  validates :due_time, presence: true
  validates :description, presence: true
  validates :status, presence: true

  belongs_to :user
end
