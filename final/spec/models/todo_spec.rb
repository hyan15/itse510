require 'rails_helper'

RSpec.describe Todo, type: :model do
  it "should have date, due_time, description, status attributes" do
    todo = Todo.new
    expect(todo.respond_to?(:date)).to be_truthy
    expect(todo.respond_to?(:due_time)).to be_truthy
    expect(todo.respond_to?(:description)).to be_truthy
    expect(todo.respond_to?(:status)).to be_truthy
  end

  it "date, due_time, description, status attributes are required" do
    todo = Todo.new
    expect(todo.save).to be_falsy

    todo.description = 'Todo'
    expect(todo.save).to be_falsy

    todo.date = DateTime.now
    expect(todo.save).to be_falsy

    todo.due_time = DateTime.now
    expect(todo.save).to be_falsy

    todo.status = :created
    expect(todo.save).to be_falsy
  end

  it "status should have one of two values - created and done" do
    expect(Todo.statuses.size).to eq 2
    [:created, :done].each {|status| expect(Todo.statuses.has_key?(status))}
  end
end
