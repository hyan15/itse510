require 'rails_helper'

RSpec.describe User, type: :model do
  it "email is required for register" do
    user = User.new
    expect(user.save).to be_falsy
  end

  it "password is required and must at least 6 characters containing at least one number, one lowercase letter" do
    user = User.new(email: 'blabla@example.org')
    expect(user.save).to be_falsy

    user.password = 'bla'
    user.password_confirmation = 'bla'
    expect(user.save).to be_falsy

    user.password = 'blabla'
    user.password_confirmation = 'blabla'
    expect(user.save).to be_falsy

    user.password = 'BLABLA'
    user.password_confirmation = 'BLABLA'
    expect(user.save).to be_falsy

    user.password = '111111'
    user.password_confirmation = '111111'
    expect(user.save).to be_falsy

    user.password = 'blabla1'
    user.password_confirmation = 'blabla1'
    expect(user.save).to be_truthy
  end
end
