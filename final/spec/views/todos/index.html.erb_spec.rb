require 'rails_helper'

RSpec.describe "todos/index", type: :view do
  login_user

  before(:each) do
    now = DateTime.now
    assign(:todos, [
      Todo.create!(
        :description => "Task 1",
        :date => now,
        :due_time => now,
        :status => :created,
        :user => controller.current_user
      ),
      Todo.create!(
        :description => "Task 2",
        :date => now,
        :due_time => now,
        :status => :created,
        :user => controller.current_user
      )
    ])
  end

  it "renders a list of todos" do
    render

    assert_select "table#todo-list > tbody > tr" do
      assert_select "td" do |todo_fields|
        expect(todo_fields[0].text).to match('Task')
        expect(todo_fields[3].text).to eq 'created'
      end
    end
  end
end
