require 'rails_helper'

RSpec.describe "todos/show", type: :view do
  login_user

  before(:each) do
    now = DateTime.now
    @todo = assign(:todo, Todo.create!(
      :description => "Description",
      :date => now,
      :due_time => now,
      :status => :created,
      :user => controller.current_user
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Description/)
    expect(rendered).to match(/created/)
  end
end
