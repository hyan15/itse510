require 'rails_helper'

RSpec.describe "todos/edit", type: :view do
  login_user

  before(:each) do
    now = DateTime.now
    @todo = assign(:todo, Todo.create!(
      :date => now,
      :due_time => now,
      :description => "MyString",
      :status => :created,
      :user => controller.current_user
    ))
  end

  it "renders the edit todo form" do
    render

    assert_select "form[action=?][method=?]", todo_path(@todo), "post" do

      assert_select "input[name=?]", "todo[date]"

      assert_select "input[name=?]", "todo[due_time]"

      assert_select "input[name=?]", "todo[description]"

      assert_select "select[name=?]", "todo[status]"
    end
  end
end
