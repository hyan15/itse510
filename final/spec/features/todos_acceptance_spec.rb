require 'rails_helper'

feature "Todos Operations" do
  context 'Without user login' do
    scenario "Todos are only available after signin" do
      visit '/'
      expect(page).to have_current_path(new_user_session_path)

      visit new_todo_path
      expect(page).to have_current_path(new_user_session_path)
    end
  end

  context 'With user login' do
    let(:user) { FactoryBot.create(:user) }
    let(:user2) { FactoryBot.create(:user) }

    before(:each) do
      login_as(user, :scope => :user)
    end

    after(:each) do
      Warden.test_reset!
    end

    scenario "Logged in user can create todo" do
      visit new_todo_path
      expect(page).to have_current_path(new_todo_path)
    end

    scenario "User can only see his own todos" do
      date = DateTime.new
      due_date = date.end_of_day

      common_params = {
        date: date,
        due_time: due_date,
        status: :created
      }

      todo1 = {user: user, description: 'Task 1'}
      Todo.create!(todo1.merge(common_params))

      todo2 = {user: user2, description: 'Task 2'}
      Todo.create!(todo1.merge(common_params))

      visit '/'
      expect(page).to have_content('Task 1')
      expect(page).to_not have_content('Task 2')
    end

    scenario "Todo list should be sorted by due time on todo list page", driver: :selenium_chrome do
      common_params = {
        date: DateTime.new,
        description: 'Task',
        status: :created,
        user: user
      }

      Todo.create!({due_time: 1.day.from_now}.merge(common_params))
      Todo.create!({due_time: 2.day.from_now}.merge(common_params))
      Todo.create!({due_time: 3.day.from_now}.merge(common_params))

      visit '/'
      sleep 2

      due_times = page.all(:xpath, './/table[@id="todo-list"]/tbody/tr/td[2]')
      expect(due_times.size).to eq 3

      datetimes = []
      due_times.each do |due_time|
        datetimes << DateTime.strptime(due_time.text, '%Y-%m-%d %H:%M:%S')
      end
      expect(datetimes.sort).to eq datetimes
    end

    scenario "User can mark a todo item as done and reverse the action" do
      date = DateTime.new
      due_date = date.end_of_day
      todo_params = {
        user: user,
        description: 'Task',
        date: date,
        due_time: due_date,
        status: :created
      }

      todo = Todo.create!(todo_params)

      visit edit_todo_path(todo)
      page.select :done, from: 'todo[status]'
      page.click_button 'Update Todo'

      expect(page).to have_current_path(todo_path(todo))
      expect(page).to have_content(:done)

      visit edit_todo_path(todo)
      page.select :created, from: 'todo[status]'
      page.click_button 'Update Todo'

      expect(page).to have_current_path(todo_path(todo))
      expect(page).to have_content(:created)
    end

    scenario "There should be some indicator to tell if a todo item is done or not" do
      date = DateTime.new
      due_date = date.end_of_day
      todo_params = {
        user: user,
        description: 'Task',
        date: date,
        due_time: due_date,
        status: :created
      }

      todo = Todo.create!(todo_params)

      visit '/'

      expect(page).to have_content(:created)
    end
  end
end
