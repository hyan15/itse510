require 'factory_bot'

FactoryBot.define do
  factory :user do
    sequence(:email) {|n| "fake#{n}@example.org" }
    # email { "fake@example.org" }
    password { "1qaz2wsx" }
    password_confirmation { "1qaz2wsx" }
  end
end

module ControllerMacros
  def login_user
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      user = FactoryBot.create(:user)
      sign_in user
    end
  end
end
