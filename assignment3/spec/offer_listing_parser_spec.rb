require_relative '../lib/offer_listing_parser'

RSpec.describe OfferListingParser do
  describe 'Should parse amazon offer listing page' do
  	offer_listing_html = File.read(File.join(File.dirname(__FILE__), 'data/offer_listing.htm'))
  	offer_page = OfferListingParser.parse(offer_listing_html)

  	it 'should find 10 offers' do
  		expect(offer_page.offers.size).to eq(10)
  	end

  	it 'should find offer owlsbooks' do
  		owlsbooks_offer = offer_page.offers.find {|offer| offer.seller.seller_name == 'owlsbooks'}
  		expect(owlsbooks_offer).to_not be_nil

  		seller = owlsbooks_offer.seller
  		expect(seller.seller_id).to eq('A1TJP2EKS10VL4')
  		expect(seller.feedback).to eq(623885)
  		expect(seller.ratings).to eq(95)

  		price = owlsbooks_offer.price
  		expect(price.sale_price).to eq(5.99)
  		expect(price.shipping_fee).to eq(0)

  		condition = owlsbooks_offer.condition
  		expect(condition.condition).to eq('Used')
  		expect(condition.sub_condition).to eq('Good')
  	end

  	it 'shoudl find offer ALEXTHEFATDAWG-US' do
  		offer = offer_page.offers.find {|offer| offer.seller.seller_name == 'ALEXTHEFATDAWG-US' }
  		expect(offer).to_not be_nil

  		seller = offer.seller
  		expect(seller.seller_id).to eq('A2RFJF90LNL7PE')
  		expect(seller.feedback).to eq(7096)
  		expect(seller.ratings).to eq(93)

  		price = offer.price
  		expect(price.sale_price).to eq(2.00)
  		expect(price.shipping_fee).to eq(3.99)

  		condition = offer.condition
  		expect(condition.condition).to eq('Used')
  		expect(condition.sub_condition).to eq('Good')
  	end
  end
end