class OfferPage
	attr_accessor :offers

	def initialize
		@offers = []
	end

	def add_offer(offer)
		@offers.push(offer)
	end
end
