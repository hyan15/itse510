class Price
	attr_accessor :sale_price, :shipping_fee

	def initialize(sale_price, shipping_fee)
		@sale_price = sale_price
		@shipping_fee = shipping_fee
	end
end
