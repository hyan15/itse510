class Condition
	attr_accessor :condition, :sub_condition

	def initialize(condition, sub_condition)
		@condition = condition
		@sub_condition = sub_condition
	end
end
