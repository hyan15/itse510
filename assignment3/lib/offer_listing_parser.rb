require 'rubygems'
require 'nokogiri'
require 'byebug'

require 'offer_page'
require 'offer'
require 'seller'
require 'price'
require 'condition'

class OfferListingParser
	def self.parse(offer_html)
		offer_page = OfferPage.new

		html = Nokogiri::HTML(offer_html)

		offer_nodes = html.xpath('.//div[@id="olpOfferList"]//div[contains(@class, "olpOffer")]')
		offer_nodes.each {|offer_node| offer_page.add_offer(self.parse_offer(offer_node)) }

		offer_page
	end

	def self.parse_offer(offer_node)
		offer = Offer.new
		offer.seller = self.parse_seller(offer_node)
		offer.price = self.parse_price(offer_node)
		offer.condition = self.parse_condition(offer_node)

		offer
	end

	def self.parse_seller(offer_node)
		seller = {seller_name: 'Amazon', seller_id: nil, feedback: nil, ratings: nil}

		seller_name_image = offer_node.xpath(
			'./div[contains(@class, "olpSellerColumn")]/*[contains(@class, "olpSellerName")]//img')
		if seller_name_image.size > 0
			seller[:seller_name] = 'Amazon'
		else
			seller[:seller_name] = offer_node.xpath(
				'./div[contains(@class, "olpSellerColumn")]/*[contains(@class, "olpSellerName")]//a/text()').first.content.strip

			seller_url = offer_node.xpath(
				'./div[contains(@class, "olpSellerColumn")]/*[contains(@class, "olpSellerName")]//a/@href').first
			unless seller_url.nil?
				matched_data = /&seller=([A-Z0-9]+)/.match(seller_url)
				unless matched_data.nil?
					if matched_data.size > 1
						seller[:seller_id] = matched_data[1]
					end
				end
			end

			ratings_node = offer_node.xpath(
				'./div[contains(@class, "olpSellerColumn")]/p/a/b/text()').first
			unless ratings_node.nil?
				begin
					seller[:ratings] = ratings_node.content.strip.split.first.gsub('%', '').to_i
				rescue
					seller[:ratings] = nil
				end
			end

			seller_desc = offer_node.xpath(
				'./div[contains(@class, "olpSellerColumn")]/p/text()').map {|node| node.content.strip }.join
			if seller_desc.size > 0
				matched_data = /.*\(([0-9\.,]+) total ratings\)/.match(seller_desc)
				unless matched_data.nil?
					if matched_data.size > 1
						seller[:feedback] = matched_data[1].gsub(/,/, '').to_i
					end
				end
			end
		end

		Seller.new(seller[:seller_name], seller[:seller_id], seller[:feedback], seller[:ratings])
	end

	def self.parse_price(offer_node)
		product_price = 0
		shipping_price = 0

		product_price_str = offer_node.xpath(
			'./div[contains(@class, "olpPriceColumn")]/span[contains(@class, "olpOfferPrice")]/text()').first
		unless product_price_str.nil?
			product_price = product_price_str.content.strip.gsub(/[^0-9\.]/, '').strip.to_f
		end

		prime_node = offer_node.xpath(
			'./div[contains(@class, "olpPriceColumn")]/span[contains(@class, "supersaver")]').first
		free_shipping_str = offer_node.xpath(
			'./div[contains(@class, "olpPriceColumn")]/p[@class="olpShippingInfo"]/span[@class="a-color-secondary"]/b/text()').first
		unless prime_node or (free_shipping_str and free_shipping_str.content.downcase.include?('free'))
			shipping_price_str = offer_node.xpath(
				'./div[contains(@class, "olpPriceColumn")]/p[@class="olpShippingInfo"]//span[@class="olpShippingPrice"]/text()').first
			unless shipping_price_str.nil?
				shipping_price = shipping_price_str.content.strip.gsub(/[,\$]/, '').to_f
			end
		end

		Price.new(product_price, shipping_price)
	end

	def self.parse_condition(offer_node)
		condition_node = offer_node.xpath(
			'./div[contains(@class, "olpConditionColumn")]//span[contains(@class, "olpCondition")]').first
		condition_strs = condition_node.xpath('.//text()').map {|node| node.content }
		condition_str = condition_strs.join
		condition_str_downcase = condition_str.downcase
		if condition_str_downcase.include?('new')
			condition = 'New'
			subcondition = 'New'
		elsif condition_str_downcase.include?('collectible')
			condition = 'Collectible'
			subcondition = condition_node.xpath(
			    './span[@id="offerSubCondition"]/text()').first.content.strip
		else
			parts = condition_str.split('-').map {|part| part.strip }
			condition = parts.first
			subcondition = parts.last
		end

		Condition.new(condition, subcondition)
	end
end
