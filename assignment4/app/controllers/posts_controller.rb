class PostsController < ApplicationController
	def index
		@posts = Post.all
	end

	def new
		@post = Post.new
		@categories = Category.all
	end

	def create
		@post = Post.new(post_params)
		if params[:post].has_key?(:category) and !params[:post][:category].empty?
			@post.category = Category.find_by(id: params[:post][:category].to_i)
		end

		if @post.save
      redirect_to post_url(@post)
    else
    	@categories = Category.all
      render 'new'
    end
	end

	def edit
		@post = Post.find_by(id: params[:id])
		@categories = Category.all
	end

	def update
		@post = Post.find_by(id: params[:id])
		if params[:post].has_key?(:category) and !params[:post][:category].empty?
			@post.category = Category.find_by(id: params[:post][:category].to_i)
		end

		if @post.update(post_params)
      redirect_to post_url(@post)
		else
    	@categories = Category.all
      render 'edit'
		end
	end

	def show
		@post = Post.find_by(id: params[:id])
	end

	def destory
    @post = Post.find_by(id: params[:id])
    @post.destroy
    redirect_to posts_url
	end

  private
    def post_params
      params.require(:post).permit(:title, :description)
    end
end
