class Category < ApplicationRecord
	has_many :posts

	validates :name, presence: true, length: { maximum: 500 }
end
