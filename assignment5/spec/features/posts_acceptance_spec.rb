require 'rails_helper'

feature "Post Operations" do
	scenario "Home page should be post list page" do
		visit '/'
		expect(page).to have_xpath('//body/h1')
		expect(page).to have_content('My Blog')
	end

	scenario "New Post link should exist on post list page and should point to post create page" do
		visit '/'
		expect(page).to have_content('New Post')
		expect(page).to have_link('New Post', href: '/posts/new')
	end

	scenario "should go to post show page when post has been created" do
		visit '/posts/new'

		fill_in 'Title', with: 'Fist Post'
		fill_in 'Description', with: 'This is my first post.'
		click_button 'Create Post'

		expect(page).to have_content('Post was successfully created.')
	end

	scenario "should display error when post creation fails" do
		visit '/posts/new'

		fill_in 'Title', with: ''
		click_button 'Create Post'

		expect(page).to have_content('prohibited this post from being saved')
	end

	scenario "should go to post show page when post has been updated" do
		post = Post.create(title: 'Prepared Post', description: 'This is a prepared post')

		visit edit_post_path(post)

		fill_in 'Title', with: 'Prepared Post Updated Title'
		click_button 'Update Post'

		expect(page).to have_content('Post was successfully updated.')
		expect(page).to have_content('Prepared Post Updated Title')

		post.destroy
	end

	scenario "should display error when post update fails" do
		post = Post.create(title: 'Prepared Post', description: 'This is a prepared post')

		visit edit_post_path(post)

		fill_in 'Title', with: ''
		click_button 'Update Post'

		expect(page).to have_content('prohibited this post from being saved')

		post.destroy
	end

	scenario "should display confirmation window before deletion", driver: :selenium_chrome do
		post = Post.create(title: 'Prepared Post', description: 'This is a prepared post')

		visit '/'

		accept_prompt do
			click_link 'Destroy'
		end
		sleep 1

		expect(page).to have_no_content('Prepared Post')
	end
end
