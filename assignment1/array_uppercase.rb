# -*- coding: utf-8 -*-

# Copyright © 2018 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email:   ibprnd@gmail.com

def array_uppercase(arr_str)
  arr_str.map {|str| str.upcase }
end

print array_uppercase(['hello', 'world'])