# -*- coding: utf-8 -*-

# Copyright © 2018 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email:   ibprnd@gmail.com

class Student
  attr_reader :name, :age, :program, :major

  def initialize(name, age, program, major)
    @name = name
    @age = age
    @program = program
    @major = major
  end

  def display
    '#{%{name}}-#{%{age}}-#{%{program}}-#{%{major}}' % {name: @name, age: @age, program: @program, major: @major}
  end
end

student = Student.new("Neal", 10, 'OIT', 'MAIT')
print student.display