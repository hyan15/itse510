# -*- coding: utf-8 -*-

# Copyright © 2018 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email:   ibprnd@gmail.com

def sum(arr_int)
  arr_int.inject {|sum, n| sum + n }
end

print sum((1..6))