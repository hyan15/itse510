# -*- coding: utf-8 -*-

# Copyright © 2018 by IBPort. All rights reserved.
# @Author: Neal Wong
# @Email:   ibprnd@gmail.com

require './student.rb'

def sort_students(students, sort_attr)
  students.sort do |s1, s2|
    if s1.respond_to?(sort_attr)
      0
    else
      s1.send(sort_attr) <=> s2.send(sort_attr)
    end
  end
end

sorted = sort_students([
    Student.new('Noel', 20, 'MDIV', 'Theology'),
    Student.new('John', 21, 'MA', 'IT'),
    Student.new('Ablaham', 22, 'BA', 'Design'),
    Student.new('Peter', 23, 'PHD', 'Ecology')
  ], :major)

sorted.each {|s| print s.display }