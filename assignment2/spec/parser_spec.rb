require_relative '../lib/parser'

RSpec.describe AmazonSellerParser do
  describe 'Should parse seller information' do
    it 'should parse normal seller' do
    	normal_seller_html = File.read(File.join(File.dirname(__FILE__), 'data/seller-normal.html'))

    	seller = AmazonSellerParser.parse(normal_seller_html)
      expect(seller[:seller_name]).to eq('D M Books Store')
      expect(seller[:seller_id]).to eq('A19A29L7PVBL05')
      expect(seller[:ratings]).to eq(92)
      expect(seller[:feedback]).to eq(409)
    end

    it 'should parse amazon' do
    	amazon_html = File.read(File.join(File.dirname(__FILE__), 'data/seller-amazon.html'))
    	seller = AmazonSellerParser.parse(amazon_html)
    	expect(seller[:seller_name]).to eq('Amazon')
    	expect(seller[:seller_id]).to be_nil
    	expect(seller[:ratings]).to be_nil
    	expect(seller[:feedback]).to be_nil
    end

    it 'should parse just launched seller' do
    	just_launched_seller_html = File.read(File.join(File.dirname(__FILE__), './data/seller-just-launched.html'))
    	seller = AmazonSellerParser.parse(just_launched_seller_html)
    	expect(seller[:seller_name]).to eq('Kathysshop')
    	expect(seller[:seller_id]).to eq('AYXDDSK9W0RO9')
    	expect(seller[:ratings]).to be_nil
    	expect(seller[:feedback]).to be_nil
    end
  end
end