class Seller
	attr_accessor :seller_name, :seller_id, :feedback, :ratings

	def initialize(seller_name, seller_id = nil, feedback = nil, ratings = nil)
		@seller_name = seller_name
		@seller_id = seller_id
		@feedback = feedback
		@ratings = ratings
	end
end