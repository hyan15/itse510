require 'rubygems'
require 'nokogiri'

class AmazonSellerParser
	def self.parse(seller_html)
		seller = {seller_name: 'Amazon', seller_id: nil, feedback: nil, ratings: nil}

		html = Nokogiri::HTML(seller_html)

		seller_name_image = html.xpath(
			'//div[contains(@class, "olpSellerColumn")]/*[contains(@class, "olpSellerName")]//img')
		if seller_name_image.size > 0
			seller[:seller_name] = 'Amazon'
			return seller
		end

		seller[:seller_name] = html.xpath(
			'//div[contains(@class, "olpSellerColumn")]/*[contains(@class, "olpSellerName")]//a/text()').first.content.strip

		seller_url = html.xpath(
			'//div[contains(@class, "olpSellerColumn")]/*[contains(@class, "olpSellerName")]//a/@href').first
		unless seller_url.nil?
			matched_data = /&seller=([A-Z0-9]+)/.match(seller_url)
			unless matched_data.nil?
				if matched_data.size > 1
					seller[:seller_id] = matched_data[1]
				end
			end
		end

		ratings_node = html.xpath(
			'//div[contains(@class, "olpSellerColumn")]/p/a/b/text()').first
		unless ratings_node.nil?
			begin
				seller[:ratings] = ratings_node.content.strip.split.first.gsub('%', '').to_i
			rescue
				seller[:ratings] = nil
			end
		end

		seller_desc = html.xpath(
			'//div[contains(@class, "olpSellerColumn")]/p/text()').map {|node| node.content.strip }.join
		if seller_desc.size > 0
			matched_data = /.*\(([0-9\.,]+) total ratings\)/.match(seller_desc)
			unless matched_data.nil?
				if matched_data.size > 1
					seller[:feedback] = matched_data[1].to_i
				end
			end
		end

		seller
	end
end